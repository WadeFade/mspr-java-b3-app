package fr.epsi;

import generator.HTMLGenerator;
import generator.HtpasswdGenerator;
import parsing.GlobalParsing;

public class Application {


    public static void main(String[] args) throws Exception {

        if (args.length < 2)
            return;

        String folderPath = args[0];
        System.out.println(folderPath);
        String generatorPath = args[1];
        System.out.println(generatorPath);

        try {
            GlobalParsing parsing = new GlobalParsing();

            parsing.parse(folderPath);

            HTMLGenerator.generateIndexPage(parsing.getAgents(), generatorPath);

            for (int i = 0; i < parsing.getAgents().size(); i++)
            {
                HTMLGenerator.generateAgentPage(parsing.getAgents().get(i), generatorPath);
            }
            HtpasswdGenerator.generateHtpasswd(parsing.getAgents(), generatorPath);
        } catch (Exception e) {
            throw new Exception("Generator or Parsing failed check folder path or folder files");
//            System.out.println("Generator or Parsing failed");
        }
    }
}
