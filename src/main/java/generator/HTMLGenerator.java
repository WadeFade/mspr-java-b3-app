package generator;

import models.Agent;
import models.Equipment;

import java.io.*;
import java.nio.charset.StandardCharsets;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.util.List;

public class HTMLGenerator {

    public static String generateEquipementsHTML(List<Equipment> equipments) throws IOException {
        StringBuilder html = new StringBuilder("");
        String line;

        for (Equipment equipment : equipments) {
            BufferedReader br = new BufferedReader(new FileReader("./templates/equipements.html"));

            while ((line = br.readLine()) != null) {
                if (line.contains("{{name}}"))
                    line = line.replace("{{name}}", equipment.getName());
                html.append(line);
            }
        }
        return html.toString();
    }

    public static String generateAgentsHTML(List<Agent> agents) throws IOException {
        StringBuilder html = new StringBuilder("");
        String line;

        for (Agent agent : agents) {
            BufferedReader br = new BufferedReader(new FileReader("./templates/agent_info.html"));

            while ((line = br.readLine()) != null) {
                if (line.contains("{{username}}"))
                    line = line.replace("{{username}}", agent.getUsername());
                if (line.contains("{{firstname}}"))
                    line = line.replace("{{firstname}}", agent.getFirstname());
                if (line.contains("{{link}}"))
                    line = line.replace("{{link}}", agent.getFileName() + ".html");
                if (line.contains("{{image}}"))
                    line = line.replace("{{image}}", "<img src=\"/images/" + agent.getFileName() + ".jpeg\" alt=\"img\" class=\"w-full h-3/5\">");
                html.append(line);
            }
        }

        return html.toString();
    }

    public static void generateIndexPage(List<Agent> agents, String generatorPath) throws IOException {
        if (!Files.exists(Paths.get(generatorPath)))
            Files.createDirectories(Paths.get(generatorPath));

        BufferedReader br = new BufferedReader(new FileReader("./templates/index.html"));
        BufferedWriter bw = new BufferedWriter(new FileWriter(generatorPath + "index.html", StandardCharsets.UTF_8));
        String line;
        while ((line = br.readLine()) != null) {
            if (line.contains("{{agents}}"))
                line = line.replace("{{agents}}", generateAgentsHTML(agents));
            bw.write(line);
        }

        br.close();
        bw.close();
    }

    public static void generateAgentPage(Agent agent, String generatorPath) throws IOException {

        BufferedReader br = new BufferedReader(new FileReader("./templates/agent.html"));
        BufferedWriter bw = new BufferedWriter(new FileWriter(generatorPath + agent.getFileName() + ".html", StandardCharsets.UTF_8));
        String line;
        while ((line = br.readLine()) != null) {
            if (line.contains("username"))
                line = line.replace("{{username}}", agent.getUsername());
            if (line.contains("firstname"))
                line = line.replace("{{firstname}}", agent.getFirstname());
            if (line.contains("mission"))
                line = line.replace("{{mission}}", agent.getMission());
            if (line.contains("{{id}}"))
                line = line.replace("{{id}}", agent.getFileName());
            if (line.contains("{{equipements}}"))
                line = line.replace("{{equipements}}", generateEquipementsHTML(agent.getEquipments()));
            if (line.contains("{{imagetop}}"))
                line = line.replace("{{imagetop}}", "<img src=\"/images/" + agent.getFileName() + ".jpeg\" alt=\"img\"");
            if (line.contains("{{imagemid}}"))
                line = line.replace("{{imagemid}}", "<img src=\"/images/" + agent.getFileName() + ".jpeg\" alt=\"img\"");
            if (line.contains("{{imagebot}}"))
                line = line.replace("{{imagebot}}", "<img src=\"/images/" + agent.getFileName() + ".jpeg\" alt=\"img\"");
            bw.write(line);
        }

        br.close();
        bw.close();
    }
}
