package generator;

import models.Agent;

import java.io.BufferedWriter;
import java.io.FileWriter;
import java.io.IOException;
import java.security.NoSuchAlgorithmException;
import java.util.List;

public class HtpasswdGenerator {

    public static void generateHtpasswd(List<Agent> agents, String folderPath) throws IOException, NoSuchAlgorithmException {

        BufferedWriter bw = new BufferedWriter(new FileWriter(folderPath + ".htpasswd"));

        for (int i = 0; i < agents.size(); i++) {
            var result = Md5Crypt.apr1Crypt(String.valueOf(agents.get(i).getPassword()));
            String htpasswdContent = agents.get(i).getFileName() + ":" + result + "\n";
            bw.write(htpasswdContent);
        }
        bw.close();
    }
}

