package models;

import fr.epsi.Application;
import parsing.GlobalParsing;

import java.util.ArrayList;
import java.util.List;
import java.util.Objects;

public class Agent {

    String username;
    String firstname;
    String mission;
    String password;
    String fileName;
    List<Equipment> equipments = new ArrayList<>();



    public String getFileName() {
        return fileName;
    }

    public void setFileName(String fileName) {
        this.fileName = fileName;
    }

    public List<Equipment> getEquipments() {
        return equipments;
    }

    public void setEquipments(List<Equipment> equipments) {
        this.equipments = equipments;
    }

    public String getUsername() {
        return username;
    }

    public void setUsername(String username) {
        this.username = username;
    }

    public String getFirstname() {
        return firstname;
    }

    public void setFirstname(String firstname) {
        this.firstname = firstname;
    }

    public String getMission() {
        return mission;
    }

    public void setMission(String mission) {
        this.mission = mission;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    public Agent(String username, String firstname, String mission, String password, String fileName, List<String> list, List<Equipment> equipmentList)
    {
        this.username = username;
        this.mission = mission;
        this.password = password;
        this.firstname = firstname;
        this.fileName = fileName;

        for (String s : list) {
            for (Equipment equipment : equipmentList) {
                if (Objects.equals(s, equipment.getId())) {
                    this.equipments.add(equipment);
                }
            }
        }
        System.out.println(equipments.size());
    }
}
