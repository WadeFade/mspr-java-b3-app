package parsing;

import models.Agent;
import models.Equipment;

import java.io.*;
import java.nio.charset.StandardCharsets;
import java.nio.file.Files;
import java.util.ArrayList;
import java.util.List;

public class EquipementsParsing {

    public static List<Equipment> parseEquipment(File file, String folderPath) throws IOException {
        List<String> list = Files.readAllLines(file.toPath(), StandardCharsets.UTF_8);
        List<Equipment> equipments = new ArrayList<>();

        if (!file.exists())
            return null;

        for (String s : list) {
            String[] splits = s.split("\t");
            if (splits.length == 2) {
                Equipment equipment = new Equipment(splits[0], splits[1]);
                equipments.add(equipment);
            }
        }
        System.out.println("Nombre d'equipements " + equipments.size());
        return equipments;
    }
}
