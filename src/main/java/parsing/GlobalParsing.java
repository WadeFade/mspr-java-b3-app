package parsing;

import models.Agent;
import models.Equipment;

import java.io.File;
import java.io.IOException;
import java.util.List;

public class GlobalParsing
{
    List<Agent> agents;
    List<Equipment> equipments;


    public List<Agent> getAgents() {
        return agents;
    }

    public List<Equipment> getEquipments() {
        return equipments;
    }

    public void parse(String folderPath) throws Exception {
        try {
            equipments = EquipementsParsing.parseEquipment(new File(folderPath + "liste.txt"), folderPath);
            agents = AgentsParsing.parseAgent(new File(folderPath + "staff.txt"), folderPath, equipments);
        } catch (IOException e) {
            System.out.println("Debug du shlag v2: " + e);
            throw new Exception("Invalid Folder Path or missing files");
        }
    }
}
