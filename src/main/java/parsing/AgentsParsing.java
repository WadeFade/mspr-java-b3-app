package parsing;

import models.Agent;
import models.Equipment;

import java.io.*;
import java.nio.charset.StandardCharsets;
import java.nio.file.Files;
import java.util.ArrayList;
import java.util.List;

public class AgentsParsing {

    public static List<Agent> parseAgent(File file, String folderPath, List<Equipment> equipments) throws IOException {
        List<String> list = Files.readAllLines(file.toPath(), StandardCharsets.UTF_8);
        List<Agent> agents = new ArrayList<>();

        for (String s : list) {

            Agent agent;
            File fileAgent = new File(folderPath + s + ".txt");

            if (!fileAgent.exists())
                continue;

            List<String> dataAgent = Files.readAllLines(fileAgent.toPath(), StandardCharsets.UTF_8);
            agent = new Agent(dataAgent.get(0), dataAgent.get(1), dataAgent.get(2), dataAgent.get(3), s, dataAgent.subList(4, dataAgent.size()), equipments);
            agents.add(agent);
        }
        return agents;
    }
}
