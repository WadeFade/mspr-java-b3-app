package fr.epsi;

import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;


public class ApplicationTest {

    @Test
    public void testArguments()  {

        String[] args = new String[] { "hello", "world" };


        Exception thrown = Assertions.assertThrows(Exception.class, () -> {
            //Code under test
            Application.main(args);
        });

        Assertions.assertEquals("Generator or Parsing failed check folder path or folder files", thrown.getMessage());
    }

}
